import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewNoteFraisPageRoutingModule } from './new-note-frais-routing.module';

import { NewNoteFraisPage } from './new-note-frais.page';
import { AppModule } from '../app.module';
import { ApplicationPipesModule } from '../pipes/applicationPipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewNoteFraisPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [NewNoteFraisPage]
})
export class NewNoteFraisPageModule {}
