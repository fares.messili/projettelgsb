import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewNoteFraisPage } from './new-note-frais.page';

const routes: Routes = [
  {
    path: '',
    component: NewNoteFraisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewNoteFraisPageRoutingModule {}
