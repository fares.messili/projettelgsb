import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { FicheFrais } from './entites/ficheFrais';
import { Forfait } from './entites/forfait';
import { HorsForfait } from './entites/horsForfait';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthentService } from '../authent.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-new-note-frais',
  templateUrl: './new-note-frais.page.html',
  styleUrls: ['./new-note-frais.page.scss'],
})
export class NewNoteFraisPage implements OnInit {
  elemFiche: FicheFrais;
  elemForfait: Forfait;
  elemHorsForfait: HorsForfait;
  baseUrl = environment.api_url;
  moisEnCours: number = Date.now();
  readonly: boolean = false;
  nom = "";

  visiteur = this.activatedRoute.snapshot.params['idVisiteur'];

  montantEtape: string = "";
  montantKm: string = "";
  montantNuit: string = "";
  montantRepas: string = "";

  constructor(
    private http: HttpClient,
    public authentService: AuthentService,
    public authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
    this.elemFiche = {
      mois: formatDate(Date.now(), 'YYYYMM', 'fr-FR'),
      nbJustificatifs: "0",
      montantValide: "0",
      dateModif: new Date(),
      idEtat: ""
    }
    this.elemHorsForfait = {
      libelle: " ",
      date: undefined,
      montant: "0"
    }
    this.elemForfait = {
      etape: "0",
      kilometre: "0",
      nuit: "0",
      repas: "0",
      montant: 0
    }

    let idVisiteur = this.activatedRoute.snapshot.params['idVisiteur'];
    let mois = this.activatedRoute.snapshot.params['mois'];
    if (mois && idVisiteur) {
      this.initialiserFormulaireFiche(idVisiteur, mois);
    }
  }

  ngOnInit() {
  }

  initialiserFormulaireFiche(idVisiteur: string, mois: string) {

    this.authentService.getUserNom()
    this.http.get(`${this.baseUrl}consulterfichedetails`, {
      headers: { PHPSESSID: this.authentService.getSessionId() || "" },
      params: { mois: mois, idVisiteur: idVisiteur }
    }).subscribe((data: any) => {
      if (data.ficheFrais) {
        if (data.ficheFrais.idEtat !== 'CR' && !this.authentService.isComptable()) {
          this.readonly = true;
        }
        if ((data.ficheFrais.idEtat == 'RB' || data.ficheFrais.idEtat == 'VA') &&
          this.authentService.isComptable()) {
          this.readonly = true;
        }
        this.elemFiche.mois = data.ficheFrais.mois;
        this.elemFiche.nbJustificatifs = data.ficheFrais.nbJustificatifs;
        this.elemFiche.dateModif = data.ficheFrais.dateModif
        this.elemFiche.montantValide = data.ficheFrais.montantValide;
        this.elemFiche.idEtat = data.ficheFrais.idEtat;
      }
      if (data.forfait) {
        this.elemForfait.etape = data.forfait.ETP && data.forfait.ETP.quantite || 0;
        this.montantEtape = data.forfait.ETP && data.forfait.ETP.total || 0;
        this.elemForfait.kilometre = data.forfait.KM && data.forfait.KM.quantite || 0;
        this.montantKm = data.forfait.KM && data.forfait.KM.total || 0;
        this.elemForfait.nuit = data.forfait.NUI && data.forfait.NUI.quantite || 0;
        this.montantNuit = data.forfait.NUI && data.forfait.NUI.total || 0;
        this.elemForfait.repas = data.forfait.REP && data.forfait.REP.quantite || 0;
        this.montantRepas = data.forfait.REP && data.forfait.REP.total || 0;
      }
      if (data.horsForfait) {
        this.elemHorsForfait.libelle = data.horsForfait.libelle;
        this.elemHorsForfait.date = data.horsForfait.date;
        this.elemHorsForfait.montant = data.horsForfait.montant;
      }
    })
  }

  saveForfait() {
    let formData = new FormData();
    if (this.authentService.isComptable()) {
      formData.append("idVisiteur", this.activatedRoute.snapshot.params['idVisiteur']);
    } else {
      formData.append("idVisiteur", this.getUserId());
    }
    formData.append("mois", this.elemFiche.mois);
    formData.append("qteEtape", this.elemForfait.etape);
    formData.append("qteKilometre", this.elemForfait.kilometre);
    formData.append("qteNuit", this.elemForfait.nuit);
    formData.append("qteRepas", this.elemForfait.repas);

    return this.http.post(`${this.baseUrl}lignefraisforfait`, formData, { withCredentials: true })
  }

  saveHForfait() {
    let formData = new FormData();
    if (this.authentService.isComptable()) {
      formData.append("idVisiteur", this.activatedRoute.snapshot.params['idVisiteur']);
    } else {
      formData.append("idVisiteur", this.getUserId());
    }
    formData.append("mois", this.elemFiche.mois);
    formData.append("libelle", this.elemHorsForfait.libelle || " ");
    formData.append("montant", this.elemHorsForfait.montant);
    if (this.elemHorsForfait.date) {
      formData.append("date", '' + this.elemHorsForfait.date.valueOf());
    }
    return this.http.post(`${this.baseUrl}lignefraishorsforfait`, formData, { withCredentials: true })
  }
  save() {
    let formData = new FormData();
    if (this.authentService.isComptable()) {
      formData.append("montantValide", this.getMontantTotal());
      formData.append("idVisiteur", this.activatedRoute.snapshot.params['idVisiteur']);
    } else {
      formData.append("montantValide", this.elemFiche.montantValide);
      formData.append("idVisiteur", this.getUserId());
    }
    formData.append("mois", this.elemFiche.mois);
    formData.append("nbJustificatifs", this.elemFiche.nbJustificatifs);
    formData.append("dateModif", "" + this.elemFiche.dateModif.valueOf());
    formData.append("idEtat", this.elemFiche.idEtat || "CR");
    return this.http.post(`${this.baseUrl}fichefrais`, formData, { withCredentials: true })
  }

  sauvegarde() {
    this.save().subscribe((success1) => {
      this.saveForfait().subscribe((success2) => {
        this.saveHForfait().subscribe((success3) => {
          if (this.authentService.isComptable()) {
            this.router.navigate(['/comptable'])
          } else {
            this.router.navigate(['/home'])
          }
          console.log("Enregistrement réussi");
        },
          (error) => {
            console.log(error, 'Troisieme requête');
          })
      },
        (error) => {
          console.log(error, 'deuxième requête');
        })
    }, (error) => {
      console.log(error, 'première requête');
    })
  }

  getLibelleEtat() {
    switch (this.elemFiche.idEtat) {
      case "CR":
        return "Créée"
        break;
      case "CL":
        return "Clôturée"
        break;
      case "VA":
        return "Validée"
        break;
      case "RB":
        return "Remboursée"
        break;
      default:
        break;
    }

  }

  getUserId() {
    return this.authentService.getUserId() || "";
  }

  getMontantTotal() {
    return this.montantEtape + this.montantKm + this.montantNuit + this.montantRepas + parseInt(this.elemHorsForfait.montant);
  }

  newNote(){
    this.router.navigateByUrl('/new-note-frais');
  }

   logout(){
    this.authService.logout().subscribe((success)=>{
    })
  }
}

// voir importation des entités et autres comp
