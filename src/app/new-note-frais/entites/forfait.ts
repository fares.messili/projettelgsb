export interface Forfait {
    etape: string,
    kilometre: string,
    nuit: string,
    repas: string,
    montant: number
  }