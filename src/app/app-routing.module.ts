import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, CanActivate } from '@angular/router';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./auth/auth.module').then( m => m.AuthPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./consultation/home.module').then( m => m.HomePageModule),canActivate: [AuthGuard]
  },
  {
    path: 'new-note-frais',
    loadChildren: () => import('./new-note-frais/new-note-frais.module').then( m => m.NewNoteFraisPageModule)
  },
  {
    path: 'new-note-frais/:idVisiteur/:mois', 
    loadChildren: () => import('./new-note-frais/new-note-frais.module').then( m => m.NewNoteFraisPageModule),canActivate: [AuthGuard]
  },  {
    path: 'comptable',
    loadChildren: () => import('./comptable/comptable.module').then( m => m.ComptablePageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
