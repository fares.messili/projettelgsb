import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComptablePageRoutingModule } from './comptable-routing.module';

import { ComptablePage } from './comptable.page';
import { ApplicationPipesModule } from '../pipes/applicationPipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComptablePageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [ComptablePage]
})
export class ComptablePageModule {}
