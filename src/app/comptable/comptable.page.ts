import { Component, OnInit } from '@angular/core';
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import { Fiche } from '../consultation/fiche';
import { ComptableService } from './comptable.service';
import { NumberToDatePipe } from '../pipes/number-to-date.pipe';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-comptable',
  templateUrl: './comptable.page.html',
  styleUrls: ['./comptable.page.scss'],
})
export class ComptablePage implements OnInit {
  data=[];
  ficheATraiter: Fiche[] = [];
  ficheTraitees: Fiche[] = [];
  dataSource = new MatTableDataSource( );

  constructor(public comptableService: ComptableService,
    private router: Router,
    public authService: AuthService,) { }

  ngOnInit() {
    this.getFicheATraiter();
    this.getFicheTraitees();
  }

  getFicheATraiter(): void {
    this.comptableService.getFicheEtat().subscribe(
      (data: Fiche[]) => {

        this.ficheATraiter = data.filter(d => d.idEtat !== 'RB');
        this.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  getFicheTraitees(): void {
    this.comptableService.getFicheEtat().subscribe(
      (data: Fiche[]) => {

        this.ficheTraitees = data.filter(d => d.idEtat == 'RB');
        this.data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

   logout(){
    this.authService.logout().subscribe((success)=>{
    })
  }

}