import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ComptableService {
  baseUrl = environment.api_url;
  constructor(private http: HttpClient,
    private router:Router) { }

    getFicheEtat() {
      return this.http.get(`${this.baseUrl}comptable`,{withCredentials: true}).pipe(
        map((res: any) => {
          return res['data'];
        })
      );
    }
}
