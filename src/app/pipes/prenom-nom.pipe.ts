import { Pipe, PipeTransform } from '@angular/core';
import { AuthentService } from '../authent.service';

@Pipe({
  name: 'prenomNom'
})
export class PrenomNomPipe implements PipeTransform {

  constructor(private authentService: AuthentService) { }

  transform(value: unknown, ...args: unknown[]): any {
    value = this.authentService.getUserPrenom() + " " + this.authentService.getUserNom()?.toUpperCase();
    return value
  }
}