import { NgModule } from "@angular/core";
import { NumberToDatePipe } from "./number-to-date.pipe";
import { PrenomNomPipe } from "./prenom-nom.pipe";

@NgModule({
    declarations: [
      PrenomNomPipe,
    NumberToDatePipe],
   exports:[PrenomNomPipe, NumberToDatePipe]
  })
export class ApplicationPipesModule {
}