import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberToDate'
})
export class NumberToDatePipe implements PipeTransform {

  transform(value: number, ...args: any[]): any {
    const annee = String(value).slice(0, 4)
    const mois = String(value).slice(4)

    return mois + '/' + annee;
  }
}