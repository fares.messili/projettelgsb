import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentService } from './authent.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
      private router : Router,
      private AuthentService: AuthentService
      ) {
      }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.AuthentService.isLoggedIn()) {
        console.log("not connected");
        this.router.navigate(['/']); // go to login if not authenticated
        return false;
      }
      console.log("connected")
        return true;
  }

}
