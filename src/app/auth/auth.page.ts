import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { User } from './user';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  error = '';
  success = '';
  con:any;

  form: FormGroup;
  constructor(private authService: AuthService,
    private router: Router,
    private formbuilder:FormBuilder
    ) {
      this.form = this.formbuilder.group({
        login : new FormControl('', [Validators.required]),
        password: new FormControl('', Validators.required)
      })
    }

  ngOnInit() {
}

newUser(): void {
  this.authService.postNewUser(this.form.value.login,this.form.value.password).subscribe(
    (data: User) => {
      console.log("hello",data.id);
       this.con=data.id;
    },
    (err) => {
      console.log(err);
      this.error = err;
    }
  );
}
logout(){
this.authService.logout().subscribe((success)=>{
console.log("Déconnecter");

})


}
}
