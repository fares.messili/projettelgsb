import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthentService } from '../authent.service';
@Injectable({
  providedIn: 'root',
})
export class FicheService {
  baseUrl = environment.api_url;

  constructor(private http: HttpClient,
    private authentService: AuthentService) {}

  getAll() {
    return this.http.get(`${this.baseUrl}consulterfichetab`,{headers:{PHPSESSID: this.authentService.getSessionId() || ""}}).pipe(
      map((res: any) => {
        return res['data'];
      })
    );
  }
}
