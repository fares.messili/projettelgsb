import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Fiche } from './fiche';
import { FicheService } from './ficheService';
import { PrenomNomPipe } from '../pipes/prenom-nom.pipe';
import { NumberToDatePipe } from '../pipes/number-to-date.pipe';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  baseUrl = environment.api_url; 
  nom: "";
  data=[];
  fiches: Fiche[] = [];
  displayedColumns : any[] = ['id', 'mois', 'justificatifs', 'montant', 'dateModif', 'etat', 'actions']

  constructor(
    private router: Router,
    private ficheService: FicheService,
    public authService: AuthService,
  ) { }

  ngOnInit() {
    this.getFiche();
  }

  getFiche(): void {
    this.ficheService.getAll().subscribe(
      (data: Fiche[]) => {
        this.fiches = data;
        this.data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  newNote(){
    this.router.navigateByUrl('/new-note-frais');
  }

   logout(){
    this.authService.logout().subscribe((success)=>{
    })
  }
}
